FROM ubuntu:20.04

WORKDIR /emulator

RUN apt-get update -y

RUN apt-get install -y curl openjdk-16-jre-headless

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs

RUN npm install -g firebase-tools

ADD *.json *.rules .firebaserc /emulator/

RUN firebase setup:emulators:database
RUN firebase setup:emulators:firestore
RUN firebase setup:emulators:ui

ENTRYPOINT firebase emulators:start --import=/emulator/data/dump --project paidit-stage
